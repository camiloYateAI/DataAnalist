


def f1(a):
    a = a+1
    return a

def f2(areglo):
    areglo[0] = areglo[0]+1
    return areglo[0]


if __name__ == "__main__":
    a = 10
    b = [20]

    print(f1(a))
    print(f2(b))

