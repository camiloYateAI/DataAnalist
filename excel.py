from matplotlib import pyplot as py

# tupla
leanguejes = ('Python', 'C', 'Java', 'Javascript', 'C++')

# print(type(leanguejes))
slices = (100, 130, 90, 80, 128)

colores = ('red', 'blue', 'green', 'pink', 'yellow')
valores = (0.1, 0, 0, 0, 0)

_, _, texto = py.pie(slices, colors=colores, labels=leanguejes, autopct='%1.1f%%',
                     explode=valores, startangle=90)
for text in texto:
    text.set_color('white')

py.axis('equal')
py.title('Lengueajes de Programacion')
# py.legend(labels=leanguejes)

# guardar esta grafica como una imagen

py.savefig('lenguajes.png')

py.show()
